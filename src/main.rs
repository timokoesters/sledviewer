use iced::{
    button, pick_list, scrollable, Align, Button, Column, Container, Element, Length, PickList,
    Row, Sandbox, Scrollable, Settings, Space, Text,
};
use sled::{Db, IVec};
use std::fmt;

const MAX_KEYS_ON_SCREEN: usize = 50;

pub fn main() -> iced::Result {
    Example::run(Settings::default())
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct IVecString {
    pub bytes: IVec,
    pub string: String,
}

impl fmt::Display for IVecString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.string)
    }
}

impl From<IVec> for IVecString {
    fn from(bytes: IVec) -> Self {
        Self {
            string: String::from_utf8_lossy(&bytes).into_owned(),
            bytes,
        }
    }
}

struct Example {
    db: Db,
    all_trees: Vec<IVecString>,
    scroll: scrollable::State,
    pick_list: pick_list::State<IVecString>,
    selected_tree: IVecString,
    tree_keys_iterator: Box<
        dyn std::marker::Send
            + std::marker::Sync
            + std::iter::DoubleEndedIterator<Item = sled::Result<IVec>>,
    >,
    tree_keys: Vec<IVecString>,
    tree_keys_offset: usize,
    next_button: button::State,
    back_button: button::State,
    scan_len_button: button::State,
}

#[derive(Debug, Clone)]
enum Message {
    TreeSelected(IVecString),
    Back,
    Next,
    ScanLen,
}

impl Sandbox for Example {
    type Message = Message;

    fn new() -> Self {
        let mut args = std::env::args();
        let cmd = args.next().unwrap();

        let path = if let Some(path) = args.next() {
            path
        } else {
            panic!("Usage: {} <path>", cmd);
        };

        let db = sled::open(path).unwrap();
        let all_trees = db
            .tree_names()
            .into_iter()
            .map(IVecString::from)
            .collect::<Vec<_>>();

        Self {
            scroll: Default::default(),
            pick_list: Default::default(),
            selected_tree: all_trees[0].clone(),
            tree_keys_iterator: Box::new(db.open_tree(&all_trees[0].bytes).unwrap().iter().keys()),
            tree_keys: Default::default(),
            tree_keys_offset: 0,
            all_trees,
            db,
            next_button: Default::default(),
            back_button: Default::default(),
            scan_len_button: Default::default(),
        }
    }

    fn title(&self) -> String {
        String::from("Sled Viewer - Iced")
    }

    fn update(&mut self, message: Message) {
        match message {
            Message::TreeSelected(tree) => {
                self.selected_tree = tree;
                self.tree_keys_iterator = Box::new(
                    self.db
                        .open_tree(&self.selected_tree.bytes)
                        .unwrap()
                        .iter()
                        .keys(),
                );
                self.tree_keys = self
                    .tree_keys_iterator
                    .by_ref()
                    .take(MAX_KEYS_ON_SCREEN)
                    .map(|bytes| IVecString::from(bytes.unwrap()))
                    .collect();
                self.tree_keys_offset = 0;
            }
            Message::Back => {
                self.tree_keys_offset = self.tree_keys_offset.saturating_sub(MAX_KEYS_ON_SCREEN);
            }
            Message::Next => {
                if self.tree_keys_offset + MAX_KEYS_ON_SCREEN >= self.tree_keys.len() {
                    self.tree_keys.extend(
                        self.tree_keys_iterator
                            .by_ref()
                            .take(MAX_KEYS_ON_SCREEN)
                            .map(|bytes| IVecString::from(bytes.unwrap())),
                    );
                }

                if self.tree_keys_offset + MAX_KEYS_ON_SCREEN < self.tree_keys.len() {
                    self.tree_keys_offset += MAX_KEYS_ON_SCREEN;
                }
            }
            Message::ScanLen => {
                //self.db.drop_tree(&self.selected_tree.bytes).unwrap();
                println!(
                    "{}",
                    self.db.open_tree(&self.selected_tree.bytes).unwrap().len()
                );
                println!(
                    "{} MB",
                    self.db
                        .open_tree(&self.selected_tree.bytes)
                        .unwrap()
                        .iter()
                        .fold(0, |acc, e| {
                            let e = e.unwrap();
                            println!("{} -> {}", String::from_utf8_lossy(&e.0), String::from_utf8_lossy(&e.1));
                            acc + e.0.len() + e.1.len()
                        }) / 1000000 // convert to mb
                );
            }
        }
    }

    fn view(&mut self) -> Element<Message> {
        let pick_list = PickList::new(
            &mut self.pick_list,
            &self.all_trees,
            Some(self.selected_tree.clone()),
            Message::TreeSelected,
        );

        let header = Row::new().spacing(10).push(pick_list);

        let mut sledkeys = Column::new();

        for key in self
            .tree_keys
            .iter()
            .skip(self.tree_keys_offset)
            .take(MAX_KEYS_ON_SCREEN)
        {
            sledkeys = sledkeys.push(Text::new(key.to_string()));
        }

        let buttons = Row::new()
            .push(Button::new(&mut self.back_button, Text::new("Back")).on_press(Message::Back))
            .push(Button::new(&mut self.next_button, Text::new("Next")).on_press(Message::Next))
            .push(
                Button::new(&mut self.scan_len_button, Text::new("Scan len"))
                    .on_press(Message::ScanLen),
            );

        let content = Scrollable::new(&mut self.scroll)
            .width(Length::Fill)
            .align_items(Align::Start)
            .spacing(10)
            .push(header)
            .push(sledkeys)
            .push(buttons);

        Container::new(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }
}
